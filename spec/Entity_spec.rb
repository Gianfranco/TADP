require 'rspec'
require_relative '../src/Entity'

describe 'Entity Test' do

  after(:each) do
    Person.clean_all
  end

  class Person
    include Entity

    has_one String, named: :first_name
    has_one String, named: :last_name
    has_one Numeric, named: :age, default: 4
    has_one Numeric, named: :fingers, default: 2, from: 0, to: 20, validate: Proc.new { |value| value >= 2 }

  end

  it 'has_one' do
    p = Person.new
    p.first_name = 'raul'
    p.last_name = 8
    expect(p.last_name).to eq(8)
    expect(p.first_name).to eq('raul')
    expect {p.size}.to raise_error(NoMethodError)
    expect{p.save!}.to raise_error(RuntimeError)
  end

  it 'save!' do
    p = Person.new
    p.first_name = 'raul'
    p.last_name = 'porcheto'
    p.age = 8
    p.save!
    expect(p.id.nil?).to eq(false)
  end

  it 'refresh!' do
    p = Person.new
    p.first_name = 'raul'
    p.last_name = 'porcheto'
    p.save!

    p.first_name = 'pepe'
    expect(p.first_name).to eq('pepe')

    p.refresh!
    expect(p.first_name).to eq('raul')
    expect(p.age).to eq(4)

    expect {Person.new.refresh!}.to raise_error(RuntimeError)
  end

  it 'forget!' do
    p = Person.new
    p.first_name = 'arturo'
    p.last_name = 'puig'
    p.age = 99
    p.save!
    expect(p.id.nil?).to eq(false)
    p.forget!
    expect(p.id.nil?).to eq(true)
    expect{p.forget!}.to raise_error(RuntimeError)
  end

  it 'validations' do
    p = Person.new
    p.first_name = 'arturo'
    p.last_name = 'puig'
    p.fingers = -1
    expect{p.save!}.to raise_error(RuntimeError)
    p.fingers= 21
    expect{p.save!}.to raise_error(RuntimeError)
    p.fingers= 1
    expect{p.save!}.to raise_error(RuntimeError)
    p.fingers=4
    p.save!
  end

end