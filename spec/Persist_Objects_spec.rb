require 'rspec'
require_relative '../src/Entity'

context 'My context' do

  after(:each) do
   Grade.clean_all
   StudentMultiple.clean_all
   Student.clean_all
  end

  class Grade
    include Entity
    has_one Numeric, named: :value

    def self.init(value)
      grade = Grade.new
      grade.value = value
      grade
    end

    def ==(other)
      self.value == other.value
    end
  end

  class Student
    include Entity
    has_one String, named: :full_name
    has_one Grade, named: :grade
  end

  class StudentMultiple
    include Entity
    has_one String, named: :full_name
    has_many Grade, named: :grades

  end

  it 'Composition one object' do
    s = Student.new
    s.full_name = 'leo sbaraglia'
    s.grade = Grade.new
    s.grade.value = 8
    s.save!                        # Salva al estudiante Y su nota
    puts(s.id)
    expect(Grade.all_instances).to eq([s.grade])
    g = s.grade                    # Retorna Grade(8)
    expect(g.value).to eq(8)
    g.value = 5
    g.save!

    expect(s.refresh!.grade).to eq([g])               # Retorna Grade(5)
  end

  it 'Composition multiple objects' do
    s = StudentMultiple.new
    s.full_name = 'leo sbaraglia'
    g = Grade.new
    g.value = 8
    g2 = Grade.new
    g2.value = 9
    s.grades = [g, g2]
    s.save!
    expect(Grade.all_instances).to eq([g, g2])
    s.grades.each{|grade| grade.value = 0}
    s.refresh!
    expect(s.grades).to eq([Grade.init(8), Grade.init(9)])
  end


end