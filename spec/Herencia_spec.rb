require 'rspec'
require_relative '../src/Entity'


context 'My context' do

  after(:each) do
    Person.clean_all
    Client.clean_all
    Seller.clean_all
  end

  class Person
    include Entity
    has_one Integer, named: :age
    def ==(other)
      self.age == other.age
    end
  end

  class Client < Person
    has_one Integer, named: :clientId
    def ==(other)
      (self.clientId == other.clientId) && super(other)
    end
  end

  class Seller < Person
    has_one Numeric, named: :prices
    def ==(other)
      (self.prices == other.prices) && super(other)
    end
  end

  it ' save! ' do
    c = Client.new
    c.age = 5
    c.clientId = 10
    c.save!
    s = Seller.new
    s.prices = 1
    s.save!
    expect(Client.all_instances).to eq([c])
    expect(Person.all_instances).to eq([c, s])
  end

  it 'refresh!' do
    c = Client.new
    c.age = 5
    c.clientId = 10
    c.save!
    c.age = 6
    expect(c.refresh!.age).to eq(5)
  end

  it 'find by father' do
    c = Client.new
    c.age = 5
    c.clientId = 10
    c.save!
    s = Seller.new
    s.age = 5
    s.save!
    c2 = Client.new
    c2.age = 6
    c2.save!
    expect(Person.find_by_age(5)).to eq([c, s])
  end

end
