require 'rspec'
require_relative '../src/Entity'

describe 'Entity Class Test' do

  after(:each) do
   Point.clean_all
  end

  class Point
    include Entity

    has_one Numeric, named: :x
    has_one Numeric, named: :y

    def self.init(x,y)
      point = new
      point.x = x
      point.y = y
      point
    end

    def ==(other)
      (self.x == other.x) && (self.y == other.y)
    end

    def add(other)
      self.x = self.x + other.x
      self.y = self.y + other.y
    end

    def x_major_5?
      self.x > 5
    end

  end
  it 'All Instances' do
    p1 = Point.new
    p1.x = 2
    p1.y = 5
    p1.save!
    p2 = Point.new
    p2.x = 1
    p2.y = 3
    p2.save!

  # Si no salvamos p3 entonces no va a aparecer en la lista
    p3 = Point.new
    p3.x = 9
    p3.y = 7

    expect(Point.all_instances).to eq([Point.init(2,5), Point.init(1,3)])       # Retorna [Point(2,5), Point(1,3)]

    p4 = Point.all_instances.first
    p4.add(p2)
    p4.save!

    expect(Point.all_instances).to eq([Point.init(1,3),Point.init(3,8)])         # Retorna [Point(3,8), Point(1,3)]

    p2.forget!

    expect(Point.all_instances).to eq([Point.init(3,8)])        # Retorna [Point(3,8)]
    p1.forget!
    expect(Point.all_instances).to eq([])

  end

  it 'search by' do
    p1 = Point.new
    p1.x = 7
    p1.y = 5
    p1.save!

    p2 = Point.new
    p2.x = 1
    p2.y = 3
    p2.save!

    p3 = Point.new
    p3.x = 6
    p3.y = 3
    p3.save!

    expect(Point.find_by_id(p1.id)).to eq([p1])

    expect(Point.find_by_x_major_5?(true)).to eq([p1, p3])
  end

end