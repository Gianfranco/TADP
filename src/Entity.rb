require 'tadb'
require 'rspec'

module Entity

  attr_accessor :id

  module ClassMethods
    def clean_all
      self.table.clear
    end

    def fields
      @fields ||= Array.new
    end

    def sons
      @sons ||= Array.new
    end

    def inherited(klass)
      self.sons.push(klass)
      klass.fields.push(*self.fields)
    end

    def all_instances
      values = Array.new
      hashes_fields = self.table.entries
      hashes_fields.each { |hash_field|
        entity = self.new
        entity.id = hash_field[:id]
        self.fields.each { |field|
          field.set_value(hash_field, entity)
        }
        values.push(entity)
      }
      self.sons.each{|son| values.push(*son.all_instances)}
      values
    end

    def method_missing(symbol, *args, &block)
      symbol_string = symbol.to_s
      if symbol_string.start_with?('find_by_')
        attribute = symbol_string.split('find_by_')[1]
        self.find_by(attribute, args[0])
      else
        super(symbol, *args, &block)
      end

    end

    def find_by(attribute, args)
      self.all_instances.select{|element|
        element.send(attribute) == args
      }
    end

    def table
      TADB::DB.table(self.name)
    end

    def has_one (clazz, hash_name)
      name = hash_name[:named]
      self.create_field(name,Field.new(clazz, name, hash_name))
    end

    def create_field(name, field)
      self.send(:attr_accessor, name)
      self.fields.push(field)
    end

    def has_many(clazz, hash_name)
      name = hash_name[:named]
      self.create_field(name,ArrayField.new(clazz, name, self.name, hash_name))
    end
  end

  def self.included(base)
    base.extend ClassMethods
  end

  def save!
    puts(self.fields)
    hash_fields = Hash.new
    unless self.id.nil?
      self.table.delete(self.id)
      hash_fields[:'id'] = self.id
    end

    self.fields.each { |field| field.add_hash(hash_fields, self)}
    puts('Insert into: ' + self.class.name + ' value: ')
    puts(hash_fields)
    self.id = self.table.insert(hash_fields)
  end

  def refresh!
    if self.id.nil?
      raise RuntimeError.new
    end

    hash_fields = self.table.entries.select { |value|
      value[:id] == self.id
    }.first
    self.fields.each { |field| field.set_value(hash_fields, self)}
    self
  end

  def fields
    self.class.fields
  end

  def table
    self.class.table
  end

  def forget!
    if self.id.nil?
      raise RuntimeError.new
    else
      self.table.delete(self.id)
      self.id = nil
    end
  end
end

class Field

  def initialize(clazz, name, restrictions)
    @type = clazz
    @name = name
    @restrictions = restrictions
  end

  def add_hash(hash, entity)
    value = self.validate(value_or_default(entity.send(@name)))

    field_name = @name.to_s
    if @type.ancestors.include? Entity
      field_value = value.save!
      object = entity.send(field_name)
      object.id = field_value
      field_name = field_name + '_id'
    else
      field_value = value
    end
    unless field_value.nil?
      hash[field_name] = field_value
    end
  end

  def set_value(hash, entity)
    field_name = @name.to_s
    value = get_value(hash[@name], hash)
    entity.send(field_name + '=', value)
  end

  def get_value(value, hash)
    if @type.ancestors.include? Entity
      field_name_id = @name.to_s + '_id'
      id = hash[field_name_id.intern]
      @type.find_by_id(id)
    else
      value
    end
  end

  def value_or_default(value)
    value || @restrictions[:default]
  end

  def validate(value)
    unless (value.class.ancestors.include? @type) || value.nil?
      raise RuntimeError.new('Validation Error, expected : ' + @type.name + ' and receipt ' + value.class.name )
    end
    no_blank = @restrictions[:no_blank].nil? ? false : @restrictions[:no_blank]
    if no_blank && (values == '' || values.nil?)
      raise RuntimeError.new('Validation Error '+ @name.to_s + " can't blank" )
    end
    from = @restrictions[:from]
    if !from.nil? && (value < from)
      raise RuntimeError.new('Validation Error ' + @name.to_s + " can't less to " + from.to_s)
    end
    to = @restrictions[:to]
    if !to.nil? && (value > to)
      raise RuntimeError.new('Validation Error ' + @name.to_s + " can't major to " + to.to_s)
    end
    validation = @restrictions[:validate]
    if !validation.nil? && !validation.call(value)
      raise RuntimeError.new('Validation Error ' + @name.to_s)
    end
    value
  end

end

class ArrayField < Field

  attr_accessor :table

  def initialize(clazz, name, clazz_name, restrictions)
    super(clazz, name, restrictions)
    self.table = TADB::DB.table(clazz_name + '_by_' + name.to_s)
  end

  def add_hash(hash, entity)
    values = entity.send(@name)
    unless values.class.ancestors.include? Array
      raise RuntimeError.new('Validation Error, expected : Array and receipt ' + values.class.name )
    end

    values = values.map{|value| self.validate(value_or_default(value))}

    generic_id = SecureRandom.uuid
    values.each{|value|
      id = value.save!
      self.table.insert({:id => generic_id, :value => id })
    }

    hash[@name] = generic_id
  end

  def get_value(id, hash)
    hash_values = self.table.entries.select{|field| field[:id] == id}
    hash_values.map{|hash_value|
      value = @type.new
      value.id = hash_value[:value]
      value.refresh!
      value
    }
  end
end